<?php

namespace backend;

use mrstroz\wavecms\components\helpers\FontAwesome;
use Yii;
use yii\base\BootstrapInterface;

class Bootstrap implements BootstrapInterface
{

    public function bootstrap($app)
    {


        Yii::$app->params['nav']['manufacturers'] = [
            'label' => FontAwesome::icon('ellipsis-h') . 'Manufacturers',
            'url' => 'javascript: ;',
            'options' => [
                'class' => 'drop-down'
            ],
            'permission' => 'manufacturers',
            'position' => 2000,
            'items' => [
                [
                    'label' => FontAwesome::icon('ellipsis-h') . 'List',
                    'url' => ['/manufacturer/index']
                ],
            ]
        ];
        Yii::$app->params['nav']['products'] = [
            'label' => FontAwesome::icon('ellipsis-h') . 'Products',
            'url' => 'javascript: ;',
            'options' => [
                'class' => 'drop-down'
            ],
            'permission' => 'product',
            'position' => 2000,
            'items' => [
                [
                    'label' => FontAwesome::icon('ellipsis-h') . 'List',
                    'url' => ['/product/index']
                ],
            ]
        ];


    }
}