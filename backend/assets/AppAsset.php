<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        'css/animate.css',
        'css/new_style.css',
        'font-awesome/css/font-awesome.css',
        'css/style.css',
    ];
    public $js = [
        'js/inspinia.js',
        'js/popper.min.js',
//        'js/jquery-3.1.1.min.js',
        'js/plugins/metisMenu/jquery.metisMenu.js',
        'js/plugins/pace/pace.min.js',
        'js/plugins/slimscroll/jquery.slimscroll.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
