<?php

namespace backend\controllers;

use backend\models\Manufacturer;
use mrstroz\wavecms\components\grid\ActionColumn;
use mrstroz\wavecms\components\grid\EditableColumn;
use mrstroz\wavecms\components\web\Controller;
use Yii;
use yii\data\ActiveDataProvider;

class ManufacturerController extends Controller
{

    public function init()
    {
        $this->heading = Yii::t('wavecms/user', 'Manufacturers');

        /** @var Manufacturer $modelProduct */
        $manufacturerModel = Yii::createObject(Manufacturer::className());

        $this->query = $manufacturerModel::find();

        $this->dataProvider = new ActiveDataProvider([
            'query' => $this->query,
        ]);

        $this->columns = array(
            'id',
            [
                'class' => EditableColumn::className(),
                'attribute' => 'xml_url',
            ],
            [
                'class' => EditableColumn::className(),
                'attribute' => 'item_node',
            ],
            [
                'class' => EditableColumn::className(),
                'attribute' => 'name_node',
            ],
            [
                'class' => EditableColumn::className(),
                'attribute' => 'in_stock_node',
            ],
            [
                'class' => EditableColumn::className(),
                'attribute' => 'not_in_stock_value',
            ],
            [
                'class' => EditableColumn::className(),
                'attribute' => 'description_node',
            ],
            [
                'class' => EditableColumn::className(),
                'attribute' => 'manufacturer_name',
            ],
            [
                'class' => ActionColumn::className(),
            ]);
    }

}