<?php

namespace backend\controllers;

use backend\models\Manufacturer;
use backend\models\Product;
use mrstroz\wavecms\components\grid\ActionColumn;
use mrstroz\wavecms\components\grid\EditableColumn;
use mrstroz\wavecms\components\web\Controller;
use Yii;
use yii\data\ActiveDataProvider;

class ProductController extends Controller
{

    public function init()
    {
        $this->heading = Yii::t('wavecms/user', 'Products');

        $this->query = Product::find()->select([
            'product.id',
            'product.name',
            'product.manufacturer_id',
            'manufacturer_name' => 'manufacturer.manufacturer_name',
            'product.in_stock',
            'product.in_xml',
            'product.qty',
            'product.ean',
            'product.description',
            'product.created_at',
            'product.uploaded_at',
            'product.in_xml_date'
        ])->leftJoin(Manufacturer::tableName(), Product::tableName() . '.manufacturer_id = ' . Manufacturer::tableName() . '.id');

        $this->dataProvider = new ActiveDataProvider([
            'query' => $this->query,
        ]);

        $this->columns = array(

            'id',
            [
                'class' => EditableColumn::className(),
                'attribute' => 'name',
            ],
            [
                'class' => EditableColumn::className(),
                'attribute' => 'manufacturer_id',
                'value' => 'manufacturer_name'
            ],
            [
                'class' => EditableColumn::className(),
                'attribute' => 'in_stock',
                'value' => function ($model) {
                    return $model->in_stock ? 'YES' : 'NO';
                }
            ],
            [
                'class' => EditableColumn::className(),
                'attribute' => 'in_xml',
                'value' => function ($model) {
                    return $model->in_xml ? 'YES' : 'NO';
                }
            ],
            [
                'class' => EditableColumn::className(),
                'attribute' => 'qty',
            ],
            [
                'class' => EditableColumn::className(),
                'attribute' => 'ean',
            ],
            [
                'class' => EditableColumn::className(),
                'attribute' => 'description',
            ],
            [
                'class' => ActionColumn::className(),
            ]);
    }

}