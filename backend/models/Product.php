<?php

namespace backend\models;

/**
 * This is the model class for table "product".
 *
 * @property int|null $id
 * @property string|null $name
 * @property string|null $manufacturer_id
 * @property int|null $in_stock
 * @property int|null $in_xml
 * @property string|null $in_xml_date
 * @property int|null $qty
 * @property string|null $ean
 * @property string|null $description
 * @property string|null $created_at
 * @property string|null $uploaded_at
 * * @property string manufacturer_name
 */
class Product extends \yii\db\ActiveRecord
{
    public $manufacturer_name;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'in_stock', 'in_xml', 'qty', 'manufacturer_id'], 'integer'],
            [['manufacturer_id'], 'required'],
            [['description', 'name', 'manufacturer_name'], 'string'],
            [['created_at', 'uploaded_at', 'in_xml_date'], 'safe'],
            [['name', 'ean', 'description'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'manufacturer_id' => 'Manufacturer Id',
            'in_stock' => 'In Stock',
            'in_xml' => 'In Xml',
            'in_xml_date' => 'In Xml Date',
            'qty' => 'Qty',
            'ean' => 'Ean',
            'description' => 'Description',
            'created_at' => 'Created At',
            'uploaded_at' => 'Uploaded At',
            'manufacturer_name' => 'Manufacturer Name'
        ];
    }
}
