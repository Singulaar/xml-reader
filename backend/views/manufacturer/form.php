<?php

use mrstroz\wavecms\components\helpers\FormHelper;
use mrstroz\wavecms\components\helpers\WavecmsForm;
use mrstroz\wavecms\components\widgets\PanelWidget;

?>

<?php $form = WavecmsForm::begin(); ?>

<div class="row">

    <div class="col-md-6">

        <?php PanelWidget::begin(['heading' => Yii::t('wavecms/user', 'Manufacturers'), 'panel_class' => 'panel-primary']); ?>

        <?php echo $form->field($model, 'item_node'); ?>
        <?php echo $form->field($model, 'name_node'); ?>
        <?php echo $form->field($model, 'ean_node'); ?>
        <?php echo $form->field($model, 'in_stock_node'); ?>
        <?php echo $form->field($model, 'not_in_stock_value'); ?>
        <?php echo $form->field($model, 'description_node'); ?>
        <?php echo $form->field($model, 'manufacturer_name'); ?>
    </div>

    <?php PanelWidget::end(); ?>

</div>

<?php FormHelper::saveButton() ?>

<?php WavecmsForm::end(); ?>
