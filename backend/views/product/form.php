<?php

/* @var $manufacturerList array */

use backend\models\Manufacturer;
use mrstroz\wavecms\components\helpers\FormHelper;
use mrstroz\wavecms\components\helpers\WavecmsForm;
use mrstroz\wavecms\components\widgets\PanelWidget;

?>

<?php $form = WavecmsForm::begin(); ?>

<div class="row">

    <div class="col-md-6">

        <?php PanelWidget::begin(['heading' => Yii::t('wavecms/user', 'Products'), 'panel_class' => 'panel-primary']); ?>

        <?php echo $form->field($model, 'name'); ?>
        <?php echo $form->field($model, 'manufacturer_id')->dropDownList(Manufacturer::nameDropdown()); ?>
        <?php echo $form->field($model, 'in_stock')->dropDownList([1 => 'YES', 0 => 'NO']); ?>
        <?php echo $form->field($model, 'in_xml')->dropDownList([1 => 'YES', 0 => 'NO']); ?>
        <?php echo $form->field($model, 'qty')->textInput(['type' => 'number']); ?>
        <?php echo $form->field($model, 'description'); ?>
    </div>

    <?php PanelWidget::end(); ?>

</div>

<?php FormHelper::saveButton() ?>

<?php WavecmsForm::end(); ?>
