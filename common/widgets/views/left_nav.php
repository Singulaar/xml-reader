<?php

use yii\helpers\Url;

?>

<nav class="navbar-default navbar-static-side" role="navigation" style="float: left">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <span class="block m-t-xs font-bold"><?= Yii::$app->user->identity->username ?></span>
                        <span class="text-muted text-xs block">menu <b class="caret"></b></span>
                    </a>
                    <ul class="dropdown-menu animated fadeInRight m-t-xs">
                        <li><a class="dropdown-item" href="<? Url::to(['site/logout']) ?>">Logout</a></li>
                    </ul>
                </div>
                <div class="logo-element">
                    IN+
                </div>
            </li>
            <li class="<?php if (Yii::$app->controller->id == 'site' && Yii::$app->controller->action->id == 'index') {echo "active";} ?>">
                <a href="<?= Url::to(['site/index']) ?>"><i class="fa fa-th-large"></i> <span class="nav-label">Main view</span></a>
            </li>
            <li class="<?php if (Yii::$app->controller->id == 'site' && Yii::$app->controller->action->id == 'minor') {echo "active";} ?>">
                <a href="<?= Url::to(['site/minor']) ?>"><i class="fa fa-th-large"></i> <span class="nav-label">Minor view</span></a>
            </li>
            <li class="<?php if (Yii::$app->controller->id == 'read-products' && Yii::$app->controller->action->id == 'index') {echo "active";} ?>">
                <a href="<?= Url::to(['read-products/index']) ?>"><i class="fa fa-th-large"></i> <span class="nav-label">Display products</span></a>
            </li>
            <li>
                <a href="<?= Url::to(['../../backend/web']) ?>"><i class="fa fa-th-large"></i> <span class="nav-label">Admin panel</span></a>
            </li>
        </ul>
    </div>
</nav>
