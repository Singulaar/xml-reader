<?php

namespace console\controllers;

use frontend\components\helpers\XmlHelper\ProductsHelper;
use frontend\models\Manufacturer;
use yii\console\Controller;

class ProductController extends Controller
{
    public function actionIndex()
    {

        $manufacturers = Manufacturer::find()->all();

        foreach ($manufacturers as $manufacturer) {
            $helper = new ProductsHelper();
            $helper->readAndQueueProducts($manufacturer);
        }
        echo 'Done!';

    }
}
