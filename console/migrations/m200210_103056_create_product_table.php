<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%product}}`.
 */
class m200210_103056_create_product_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
       $this->createTable('{{%product}}', [
           'id' => $this->primaryKey(),
          'name' => $this->string(),
          'manufacturer_id' => $this->integer(),
          'in_stock' => $this->boolean(),
          'in_xml' => $this->boolean(),
           'qty' => $this->integer(),
          'ean' => $this->string(),
          'description' => $this->text(),
          'created_at' => $this->dateTime(),
          'uploaded_at' => $this->dateTime()
       ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%product}}');
    }
}
