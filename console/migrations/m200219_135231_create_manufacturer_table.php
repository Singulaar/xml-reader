<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%manufacturer}}`.
 */
class m200219_135231_create_manufacturer_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%manufacturer}}', [
            'id' => $this->primaryKey(),
            'xml_url' => $this->string(),
            'item_node' => $this->string(),
            'name_node' => $this->string(),
            'ean_node' => $this->string(),
            'in_stock_node' => $this->string(),
            'not_in_stock_value' => $this->string(),
            'description_node' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%manufacturer}}');
    }
}
