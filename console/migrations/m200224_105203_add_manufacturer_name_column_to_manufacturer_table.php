<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%manufacturer}}`.
 */
class m200224_105203_add_manufacturer_name_column_to_manufacturer_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%manufacturer}}', 'manufacturer_name', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%manufacturer}}', 'manufacturer_name');
    }
}
