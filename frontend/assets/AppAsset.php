<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/new_style.css',
        'css/animate.css',
        'font-awesome/css/font-awesome.css',
        'css/site.css',
        'css/style.css',
    ];
    public $js = [
        'js/inspinia.js',
        'js/popper.min.js',
//        'js/jquery-3.1.1.min.js',
        'js/plugins/metisMenu/jquery.metisMenu.js',
        'js/plugins/pace/pace.min.js',
        'js/plugins/slimscroll/jquery.slimscroll.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap4\BootstrapAsset',
    ];
}
