<?php


namespace frontend\components\helpers\XmlHelper;

use frontend\components\jobs\ProductJob;
use frontend\models\Product;
use XMLReader;
use Yii;
use yii\helpers\VarDumper;

class HalmarHelper extends XmlHelper
{
    public function prepareFile($xmlReader)
    {
        return $xmlReader->open('http://halmar.pl/halmar_stock.xml');
    }

    public function readTags($xmlReader)
    {
        $catalog = [];

        while ($xmlReader->read()) {
            //Sprawdż czy komórka nazywa się Item
            if ($xmlReader->nodeType == XMLREADER::ELEMENT && $xmlReader->localName == 'Item') {
                $item = [];
                $item['manufacturer_id'] = 1;
            }
            //Pobierz wartości
            if ($xmlReader->nodeType == XMLREADER::ELEMENT && $xmlReader->localName == 'ean') {
                $xmlReader->read();
                $item['ean'] = $xmlReader->value;
            }

            if ($xmlReader->nodeType == XMLREADER::ELEMENT && $xmlReader->localName == 'name') {
                $xmlReader->read();
                $item['name'] = $xmlReader->value;
            }

            if ($xmlReader->nodeType == XMLREADER::ELEMENT && $xmlReader->localName == 'stock') {
                $xmlReader->read();
                $item['qty'] = $xmlReader->value;

                if ($item['qty'] == 'out of stock') {
                    $item['in_stock'] = 0;
                } else {
                    $item['in_stock'] = 1;
                }

                $catalog[] = $item;
            }
        }

        return $catalog;
    }

    public function updateProduct()
    {
        $product = Product::find()->where(['manufacturer_id' => 1])->all();
        foreach ($product as $item) {
            $item->in_xml = 0;
            $item->save();
        }
    }

    public function saveDatabase($array)
    {

        if (!$array) {
            return false;
        }

        foreach ($array as $item) {

            $product = Product::find()->andWhere(['ean' => $item['ean']])->one();

            if (!$product) {
                $product = new Product();
                $product->in_xml = 1;
                $product->in_xml_date = date('Y-m-d H:m:s');
            }

            if (isset($item['ean']) && $item['ean']) {
                $product->ean = $item['ean'];
            } else {
                $product->ean = 0;
            }

            if ($product->ean == $item['ean']) {
                $product->in_xml = 1;
                $product->in_xml_date = date('Y-m-d H:m:s');
            } else {
                $product->in_xml = 0;
            }

            if (isset($item['name']) && $item['name']) {
                $product->name = $item['name'];
            }

            if (isset($item['manufacturer_id']) && $item['manufacturer_id']) {
                $product->manufacturer_id = $item['manufacturer_id'];
            } else {
                $product->manufacturer_id = '';
            }
            if (isset($item['qty']) && $item['qty']) {
                $product->qty = $item['qty'];
            } else {
                $product->qty = 0;
            }

            if (isset($item['description']) && $item['description']) {
                $product->description = $item['description'];
            } else {
                $product->description = '';
            }

            if (isset($item['in_stock']) && $item['in_stock']) {
                $product->in_stock = $item['in_stock'];
            } else {
                $product->in_stock = 0;
            }

            if (!$product->created_at) {
                $product->created_at = date('Y-m-d H:m:s');
            }
            $product->uploaded_at = date('Y-m-d H:m:s');

            $product->save(false);

        }
    }

}

