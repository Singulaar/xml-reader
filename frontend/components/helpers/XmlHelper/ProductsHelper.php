<?php


namespace frontend\components\helpers\XmlHelper;

use frontend\components\jobs\ProductJob;
use frontend\models\Manufacturer;
use frontend\models\Product;
use XMLReader;
use Yii;
use yii\helpers\VarDumper;

class ProductsHelper extends XmlHelper
{
    public function prepareFile($xmlReader, Manufacturer $manufacturer)
    {
        return $xmlReader->open($manufacturer->xml_url);
    }

    public function readTags($xmlReader, Manufacturer $manufacturer)
    {
        $catalog = [];

        while ($xmlReader->read()) {
            //Sprawdż czy komórka nazywa się Item


            if ($xmlReader->nodeType == XMLREADER::ELEMENT && $xmlReader->localName === $manufacturer->item_node) {
                $catalog[count($catalog)] = [];
                $catalog[count($catalog) - 1]['manufacturer_id'] = $manufacturer->id;
            }
            //Pobierz wartości

            if ($manufacturer->ean_node && $xmlReader->nodeType == XMLREADER::ELEMENT && $xmlReader->localName === $manufacturer->ean_node) {
                $xmlReader->read();
                $catalog[count($catalog) - 1]['ean'] = $xmlReader->value;
            }

            if ($manufacturer->name_node && $xmlReader->nodeType == XMLREADER::ELEMENT && $xmlReader->localName === $manufacturer->name_node) {
                $xmlReader->read();
                $catalog[count($catalog) - 1]['name'] = $xmlReader->value;
            }

            if ($manufacturer->description_node && $xmlReader->nodeType == XMLREADER::ELEMENT && $xmlReader->localName === $manufacturer->description_node) {
                $xmlReader->read();
                $catalog[count($catalog) - 1]['description'] = $xmlReader->value;
            }

            if ($manufacturer->in_stock_node && $xmlReader->nodeType == XMLREADER::ELEMENT && $xmlReader->localName === $manufacturer->in_stock_node) {
                $xmlReader->read();
                $catalog[count($catalog) - 1]['qty'] = $xmlReader->value;

                if ($catalog[count($catalog) - 1]['qty'] == $manufacturer->not_in_stock_value) {
                    $catalog[count($catalog) - 1]['in_stock'] = 0;
                } else {
                    $catalog[count($catalog) - 1]['in_stock'] = 1;
                }
            }
        }

        return $catalog;
    }

    public function updateProduct()
    {
        $product = Product::find()->all();
        foreach ($product as $item) {
            $item->in_xml = 0;
            $item->save();
        }
    }

    public function addToDatabase($array)
    {

        if (!$array) {
            return false;
        }

        foreach ($array as $item) {

            $product = Product::find()->andWhere(['ean' => $item['ean']])->one();


            if (!$product) {
                $product = new Product();
                $product->in_xml = 1;
                $product->in_xml_date = date('Y-m-d H:m:s');
            }


            if (isset($item['ean']) && $item['ean']) {
                $product->ean = $item['ean'];
            } else {
                $product->ean = 0;
            }

            if (isset($product->ean) && $product->ean == $item['ean']) {
                $product->in_xml = 1;
                $product->in_xml_date = date('Y-m-d H:m:s');
            } else {
                $product->in_xml = 0;
            }

            if (isset($item['name']) && $item['name']) {
                $product->name = $item['name'];
            }

            if (isset($item['manufacturer_id']) && $item['manufacturer_id']) {
                $product->manufacturer_id = $item['manufacturer_id'];
            }

            if (isset($item['qty']) && $item['qty']) {
                $product->qty = $item['qty'];
            } else {
                $product->qty = 0;
            }

            if (isset($item['in_stock']) && $item['in_stock']) {
                $product->in_stock = $item['in_stock'];
            } else {
                $product->in_stock = 0;
            }


            if (isset($item['description']) && $item['description']) {
                $product->description = $item['description'];
            } else {
                $product->description = '';
            }

            if (!$product->created_at) {
                $product->created_at = date('Y-m-d H:m:s');
            }
            $product->uploaded_at = date('Y-m-d H:m:s');

            $product->save(false);
        }
    }

    public function AddToQueue($array)
    {
        if (!$array) {
            return false;
        }
        $input_array = (array_chunk($array, 10, false));
        foreach ($input_array as $item) {
            Yii::$app->queue->push(new ProductJob([
                'items' => $item
            ]));
        }
    }
}


