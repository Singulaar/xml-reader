<?php

namespace frontend\components\helpers\XmlHelper;

use frontend\models\Manufacturer;
use XMLReader;

abstract class XmlHelper
{

    public function createReader(): XMLReader
    {
        return $xml = new XMLReader();
    }

    public function readAndSave()
    {

        $xmlReader = $this->createReader();

        $this->prepareFile($xmlReader);

        $array = $this->readTags($xmlReader);

        $this->updateProduct();

        $this->saveDatabase($array);



    }

    /**
     * @param Manufacturer $manufacturer
     */
    public function readAndAddProducts(Manufacturer $manufacturer)
    {

        $xmlReader = $this->createReader();

        $this->prepareFile($xmlReader, $manufacturer);

        $array = $this->readTags($xmlReader, $manufacturer);

        $this->updateProduct();

        $this->addToDatabase($array);

    }
    public function readAndQueueProducts(Manufacturer $manufacturer)
    {

        $xmlReader = $this->createReader();

        $this->prepareFile($xmlReader, $manufacturer);

        $array = $this->readTags($xmlReader, $manufacturer);

        $this->updateProduct();

        $this->AddToQueue($array);

    }
}

?>