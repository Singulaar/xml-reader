<?php

namespace frontend\components\jobs;

use frontend\components\helpers\XmlHelper\XmlHelper;
use frontend\models\Product;
use yii\base\BaseObject;
use yii\helpers\VarDumper;
use yii\queue\JobInterface;

class ProductJob extends BaseObject implements JobInterface


{
    public $items;

    /**
     * @inheritDoc
     */

    public function execute($queue)
    {

        foreach ($this->items as $item) {


            $product = Product::find()->andWhere(['ean' => $item['ean']])->one();


            if (!$product) {
                $product = new Product();
                $product->in_xml = 1;
                $product->in_xml_date = date('Y-m-d H:m:s');
            }


            if (isset($item['ean']) && $item['ean']) {
                $product->ean = $item['ean'];
            } else {
                $product->ean = 0;
            }


            if (isset($product->ean) && $product->ean == $item['ean']) {
                $product->in_xml = 1;
                $product->in_xml_date = date('Y-m-d H:m:s');
            } else {
                $product->in_xml = 0;
            }

            if (isset($item['name']) && $item['name']) {
                $product->name = $item['name'];
            }

            if (isset($item['manufacturer_id']) && $item['manufacturer_id']) {
                $product->manufacturer_id = $item['manufacturer_id'];
            } else {
                $product->manufacturer_id = '';
            }

            if (isset($item['qty']) && $item['qty']) {
                $product->qty = $item['qty'];
            } else {
                $product->qty = 0;
            }

            if (isset($item['in_stock']) && $item['in_stock']) {
                $product->in_stock = $item['in_stock'];
            } else {
                $product->in_stock = 0;
            }


            if (isset($item['description']) && $item['description']) {
                $product->description = $item['description'];
            } else {
                $product->description = '';
            }

            if (!$product->created_at) {
                $product->created_at = date('Y-m-d H:m:s');
            }
            $product->uploaded_at = date('Y-m-d H:m:s');

            $product->save(false);

            echo $product->name . "\n";

        }
    }
}


