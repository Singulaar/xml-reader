<?php

namespace frontend\controllers;

use frontend\components\helpers\XmlHelper\HalmarHelper;
use frontend\models\ProductSearch;
use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;


/**
 * Reader controller
 */
class ReadHalmarController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['reader', 'index'],
                'rules' => [
                    [
                        'actions' => ['index', 'reader'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionReader()
    {

        $helper = new HalmarHelper();
        $helper->readAndSave();

        return $this->redirect('index');
    }

    public function actionIndex()
    {
        $this->layout = 'table';
        $searchModel = new ProductSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->get());
        return $this->render('products-table.php', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider
        ]);
    }

}