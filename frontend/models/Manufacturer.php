<?php

namespace frontend\models;


/**
 * This is the model class for table "manufacturer".
 *
 * @property int $id
 * @property string|null $xml_url
 * @property string|null $item_node
 * @property string|null $name_node
 * @property string|null $ean_node
 * @property int|null $in_stock_node
 * @property string|null $not_in_stock_value
 * @property string|null $description_node
 */
class Manufacturer extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'manufacturer';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['in_stock_node'], 'integer'],
            [['xml_url', 'item_node', 'name_node', 'ean_node', 'not_in_stock_value', 'description_node'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'xml_url' => 'Xml Url',
            'item_node' => 'Item Node',
            'name_node' => 'Name Node',
            'ean_node' => 'Ean Node',
            'in_stock_node' => 'In Stock Node',
            'not_in_stock_value' => 'Not In Stock Value',
            'description_node' => 'Description Node',
        ];
    }
}
