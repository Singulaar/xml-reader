<?php

namespace frontend\models;

use yii\data\ActiveDataProvider;

class ProductSearch extends Product
{
    public function rules()
    {
        return [
            [['id', 'in_stock', 'in_xml', 'qty'], 'integer'],
            [['name', 'description', 'manufacturer_id'], 'string'],
            [['ean', 'in_xml_date'], 'safe'],
            [['manufacturer_name'], 'string'],
            [['created_at', 'uploaded_at'], 'safe']
        ];
    }

    public function search($params)
    {
        $query = Product::find()->select([
            'product.id',
            'product.name',
            'product.manufacturer_id',
            'manufacturer_name' => 'manufacturer.manufacturer_name',
            'product.in_stock',
            'product.in_xml',
            'product.qty',
            'product.ean',
            'product.description',
            'product.created_at',
            'product.uploaded_at',
            'product.in_xml_date'
        ])->leftJoin(Manufacturer::tableName(), Product::tableName() . '.manufacturer_id = ' . Manufacturer::tableName() . '.id');


        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'ean', $this->ean])
            ->andFilterWhere(['like', 'id', $this->id])
            ->andFilterWhere(['=', 'in_stock', $this->in_stock])
            ->andFilterWhere(['=', 'in_xml', $this->in_xml])
            ->andFilterWhere(['like', 'qty', $this->qty])
            ->andFilterWhere(['like', 'created_at', $this->created_at])
            ->andFilterWhere(['like', 'uploaded_at', $this->uploaded_at])
            ->andFilterWhere(['like', 'in_xml_date', $this->in_xml_date])
            ->andFilterWhere(['=', 'manufacturer_id', $this->manufacturer_name])
            ->andFilterWhere(['like', 'description', $this->description]);

        return $dataProvider;
    }
}