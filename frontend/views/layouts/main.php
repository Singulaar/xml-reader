<?php

/* @var $this View */

/* @var $content string */

use common\widgets\LeftNavWidget;
use common\widgets\SearchBarWidget;
use yii\helpers\Html;
use frontend\assets\AppAsset;
use yii\web\View;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>

<?php $this->beginBody() ?>
<div id="wrapper">
    <?php LeftNavWidget::begin(); ?>
    <?php LeftNavWidget::end(); ?>

    <div id="page-wrapper" class="gray-bg" style="float: left; width: 100%">
        <div class="row border-bottom">
            <?php SearchBarWidget::begin(); ?>
            <?php SearchBarWidget::end(); ?>
        </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2><?= Html::encode($this->title) ?></h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="./">Home</a>
                        </li>
                        <li class="breadcrumb-item active">
                            <strong><?= Html::encode($this->title) ?></strong>
                        </li>
                    </ol>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="text-center m-t-lg">
                        <?= $content ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer">
            <div class="pull-right">
                10GB of <strong>250GB</strong> Free.
            </div>
            <div>
                <strong>Copyright</strong> Example Company &copy; 2014-2019
            </div>
        </div>
    </div>
</div>

<?php $this->endBody() ?>
<?php $this->endPage() ?>

