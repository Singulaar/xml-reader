<?php

/* @var $item Product */

/* @var ActiveDataProvider $dataProvider */

/* @var ProductSearch $searchModel */

use frontend\models\Manufacturer;
use frontend\models\Product;
use frontend\models\ProductSearch;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\data\ActiveDataProvider;

$this->title = 'Products';

?>
<div class="content">
    <a class="btn btn-primary back-button" href="../">Back</a>
    <div class="table">
        <?= \yii\grid\GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'class' => 'product_table',
            'columns' => [
                'id',
                'name',
                [
                    'attribute' => 'manufacturer_id',
                    'value' => 'manufacturer_name',
                    'filter' => Html::activeDropDownList($searchModel, 'manufacturer_name', [''] + ArrayHelper::map(Manufacturer::find()->asArray()->all(), 'id', 'manufacturer_name'), ['class' => 'form-control']),
                ],
                [
                    'attribute' => 'in_stock',
                    'value' => function ($model) {
                        return $model->in_stock ? 'YES' : 'NO';
                    },
                    'filter' => Html::activeDropDownList($searchModel, 'in_stock', ['' => '', 1 => 'YES', 0 => 'NO'], ['class' => 'form-control']),
                ],
                [
                    'attribute' => 'in_xml',
                    'value' => function ($model) {
                        return $model->in_stock ? 'YES' : 'NO';
                    },
                    'filter' => Html::activeDropDownList($searchModel, 'in_xml', ['' => '', 1 => 'YES', 0 => 'NO'], ['class' => 'form-control']),
                ],
                'qty',
                'ean',
                'description',
            ],
            'summary' => false,
        ]) ?>
    </div>
</div>

