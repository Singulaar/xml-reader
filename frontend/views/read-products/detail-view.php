<?php

/* @var $item Product */

/* @var ActiveDataProvider $dataProvider */

/* @var ProductSearch $searchModel */

use frontend\models\Manufacturer;
use frontend\models\Product;
use frontend\models\ProductSearch;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\data\ActiveDataProvider;
use yii\widgets\DetailView;

$this->title = 'Detail';

?>
<div class="content">
    <a class="btn btn-primary back-button" href="./">Back</a>
    <div class="table">
        <?= DetailView::widget([
            'model' => $model,
            'class' => 'product_table',
            'attributes' => [
                [
                    'attribute' => 'id'
                ],
                [
                    'attribute' => 'name'
                ],
                [
                    'attribute' => 'manufacturer_id',
                    'value' => function () {
                        return $_GET['manufacturer_name'];
                    }
                ],
                [
                    'attribute' => 'in_stock',
                    'value' => function ($model) {
                        return $model->in_stock ? 'YES' : 'NO';
                    }
                ],
                [
                    'attribute' => 'in_xml',
                    'value' => function ($model) {
                        return $model->in_stock ? 'YES' : 'NO';
                    },
                ],
                [
                    'attribute' => 'in_xml_date'
                ],
                [
                    'attribute' => 'qty'
                ],
                [
                    'attribute' => 'ean'
                ],
                [
                    'attribute' => 'description'
                ],
                [
                    'attribute' => 'created_at'
                ],
                [
                    'attribute' => 'uploaded_at'
                ],

            ]]);
        ?>
    </div>
</div>

