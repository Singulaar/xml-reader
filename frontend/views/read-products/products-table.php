<?php

/* @var $item Product */

/* @var ActiveDataProvider $dataProvider */

/* @var ProductSearch $searchModel */

use frontend\models\Manufacturer;
use frontend\models\Product;
use frontend\models\ProductSearch;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\data\ActiveDataProvider;

$this->title = 'Products';

?>

<div class="content">
<!--    <a class="btn btn-primary back-button" href="--><?php //?><!--">Back</a>-->
    <div class="table">
        <?= \yii\grid\GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'class' => 'product_table',
            'columns' => [
                'id',

                [
                    'attribute' => 'name',
                    'format' => 'raw',
                    'value'=>function ($model) {
                        return Html::a($model['name'],['read-products/detail', 'id' => $model['id'], 'manufacturer_name' => $model['manufacturer_name']]);
                    },
                ],
                [
                    'attribute' => 'manufacturer_id',
                    'value' => 'manufacturer_name',
                    'filter' => Html::activeDropDownList($searchModel, 'manufacturer_name', [''=> 'not set'] + ArrayHelper::map(Manufacturer::find()->asArray()->all(), 'id', 'manufacturer_name'), ['class' => 'form-control']),
                ],
                [
                    'attribute' => 'in_stock',
                    'value' => function ($model) {
                        return $model->in_stock ? 'YES' : 'NO';
                    },
                    'filter' => Html::activeDropDownList($searchModel, 'in_stock', ['' =>'not set', 1 => 'YES', 0 => 'NO'], ['class' => 'form-control']),
                ],
                [
                    'attribute' => 'in_xml',
                    'value' => function ($model) {
                        return $model->in_stock ? 'YES' : 'NO';
                    },
                    'filter' => Html::activeDropDownList($searchModel, 'in_xml', ['' =>'not set', 1 => 'YES', 0 => 'NO'], ['class' => 'form-control']),
                ],
                'in_xml_date',
//                [
//                    'attribute' => 'in_xml_date',
//                    'value' => 'in_xml_date',
//                    'filter' => Html::activeDropDownList($searchModel, 'in_xml_date', ['',Product::inXmlDateDropdown()], ['class' => 'form-control']),
//                ],
                'qty',
                'ean',
                'description',
//                [
//                    'attribute' => 'created_at',
//                    'value' => 'created_at',
//                    'filter' => Html::activeDropDownList($searchModel, 'created_at', ['',Product::createdDateDropdown()], ['class' => 'form-control']),
//                ],
                'created_at',
//                [
//                    'attribute' => 'uploaded_at',
//                    'value' => 'uploaded_at',
//                    'filter' => Html::activeDropDownList($searchModel, 'uploaded_at', ['',Product::uploadedDateDropdown()], ['class' => 'form-control']),
//                ],
                'uploaded_at',
            ],
            'summary' => false,
        ]) ?>
    </div>
</div>

