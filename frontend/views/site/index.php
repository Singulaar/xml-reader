<?php

/* @var $this yii\web\View */
/* @var $file ReadHalmarController */

$this->title = 'XML Reader';

use yii\helpers\Url;
use frontend\controllers\ReadHalmarController;

?>

<div class="jumbotron">
    <div class="buttons">
        <p><a class="btn btn-lg btn-success" href="<?= Url::to(['read-halmar/reader']) ?>">Upload Halmar </a></p>
        <p><a class="btn btn-lg btn-success" href="<?= Url::to(['read-eurofirany/reader']) ?>">Upload Eurofirany </a></p>
    </div>
</div>




